#!/bin/bash

docker-compose up -d

echo "Waiting for starting..."
sleep 10

docker-compose ps

echo "Spark cluster is started"

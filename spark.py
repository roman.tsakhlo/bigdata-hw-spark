from pyspark.sql import SparkSession

spark = SparkSession.builder.appName("ReadCSV").getOrCreate()
spark.version

csv_path = "tweets.csv"
df = spark.read.csv(csv_path, header=True, inferSchema=True)

print("FILE DATA", df)

row_count = df.count()
print("RESULT", row_count)

spark.stop()
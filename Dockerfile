FROM bitnami/spark:latest

COPY /tweets.csv /opt/bitnami/spark/tweets.csv
COPY requirements.txt /opt/bitnami/spark/requirements.txt

RUN pip3 install --no-cache-dir -r /opt/bitnami/spark/requirements.txt

COPY /spark.py /opt/bitnami/spark/spark.py

CMD ["/opt/bitnami/spark/bin/spark-submit", "/opt/bitnami/spark/spark.py"]